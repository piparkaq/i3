/* global router: true */
/**
 * @fileoverview
 * @module Pages/History/HistoryEntry
 * @since 0.1.0
 */
'use strict';

var moment = require('moment');

/**
 * @type {Object}
 * @prop {IObservable} Parameter
 */
var ctx = this;

module.exports.entry = ctx.Parameter.map(function (e) {
  var __createdAt = moment(e.createdAt);
  return Object.assign({}, e, {
    __createdAt: __createdAt.format(),
    __createdAtFromNow: __createdAt.fromNow()
  });
});

module.exports.actions = {
  goBack: function () {
    router.goBack();
  }
};
