/* global router: true */
/**
 * @fileoverview
 * @module Pages/History/HistoryMain
 * @since 0.1.0
 *
 * @todo Divide entry list by upload date (yesterday, last week, month, the rest)
 */
'use strict';

var R = require('ramda');
var moment = require('moment');

/**
 * @alias module:Modules/Context
 */
var Context = require('Modules/Context');

/**
 * @type {IObservable<HistoryEntryModel>}
 * @alias module:Pages/History/HistoryMain.entries
 */
var entries = module.exports.entries = Context.entries.map(function (entry) {
  return Object.assign({}, entry, {
    __createdAt: moment(entry.createdAt).format()
  });
});

/**
 * @type {{
 *  entryCount: IObservable<number>,
 *  entryWord: IObservable<string>
 * }}
 */
module.exports.model = {
  entryCount: entries.count().map(R.identity),
  entryWord: entries.count().map(R.cond([
    [R.equals(1), R.always('entry')],
    [R.T, R.always('entries')]
  ]))
};

/**
 * @type {{
 *  goToUpload: function,
 *  goToEntry: function
 * }}
 */
module.exports.actions = {
  /**
   * Navigate to the Upload main page
   */
  goToUpload: function Pages$HistoryMain$actions$goToUpload () {
    router.push('upload', {}, 'main');
  },
  /**
   * Navigate to the selected entry's detail page
   * @param sender
   */
  goToEntry: function Pages$HistoryMain$actions$goToEntry (sender) {
    var id = sender.data.id;
    entries.forEach(function (it, idx) {
      var entry = entries.getAt(idx);
      if (entry.id === id) {
        router.push('history', {}, 'viewEntry', entry);
      }
    });
  }
};
