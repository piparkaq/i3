/* global router: true */
/**
 * @fileoverview
 * @module Pages/Upload/UploadResult
 * @namespace Pages.Upload.UploadResult
 * @since 0.1.0
 */
'use strict';

var R = require('ramda');
var InterApp = require('FuseJS/InterApp');
var UploadContext = require('Modules/UploadContext');

var defaultData = R.defaultTo({ data: {} });

var result = this.Parameter;

/**
 * @type {IObservable}
 * @alias module:Pages/Upload/UploadResult.data
 */
var data = module.exports.data = result.map(function (o) {
  return defaultData(o).data;
});

/**
 * @type {{
 *  reset: function
 *  launchUri: function
 * }}
 * @alias module:Pages/Upload/UploadResult
 */
var actions = module.exports.actions = {
  reset: function () {
    UploadContext.actions.clearContext();
    router.goto('upload', {}, 'main', {});
  },
  launchUri: function () {
    InterApp.launchUri(data.value.link);
  }
};
