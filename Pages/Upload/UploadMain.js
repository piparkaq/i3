/* global router: true */
/**
 * @fileoverview
 * @module Pages/Upload/UploadMain
 * @since 0.1.0
 */
'use strict';

var UploadContext = require('Modules/UploadContext');

module.exports.actions = {
  selectImage: function () {
    UploadContext.actions
                 .selectImage()
                 .then(function (data) {
                   console.log('Selected image: ' + JSON.stringify(data, null, 2));
                   router.push('upload', {}, 'confirm', data);
                 });
  }
};
