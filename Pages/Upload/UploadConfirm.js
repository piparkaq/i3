/* global router: true */
/**
 * @fileoverview
 * @module Pages/Upload/UploadConfirm
 * @since 0.1.0
 */
'use strict';

var R = require('ramda');
var Observable = require('FuseJS/Observable');
var Context = require('Modules/Context');
var Remote = require('Modules/Remote');
var UploadContext = require('Modules/UploadContext');
var Utils = require('Modules/Utils');

var logError = Utils.logError;

/** @type {IObservable} */
var image = UploadContext.image;

/** @type {IObservable} */
var uploadedImage = UploadContext.uploadedImage;

var isUploading = Observable(false);

module.exports = {
  image: image,
  uploadedImage: uploadedImage,
  isUploading: isUploading,

  flags: {
    somethingIsFunny: image.count().map(R.equals(0))
  },

  actions: {
    reset: function () {
      console.log('Should reset context');
      UploadContext.actions.clearContext();
      router.goBack();
    },
    upload: function () {
      isUploading.value = true;
      Remote.actions
            .upload(image.value)
            .then(function (json) {
              uploadedImage.value = json.data;
            })
            .then(function (json) {
              return Context.actions
                            .createEntry({
                              originalData: uploadedImage.value
                            });
            })
            .then(function (json) {
              isUploading.value = false;
              setTimeout(function () {
                router.goto('upload', {}, 'result', { data: uploadedImage.value });
              }, 500);
            })
            .catch(logError('Error while uploading.'));
    }
  }
};

