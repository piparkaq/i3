/* global router: true */
/**
 * @fileoverview
 * @module Pages/Upload
 * @since 0.1.0
 */
'use strict';

var R = require('ramda');
var Observable = require('FuseJS/Observable');
var CameraRoll = require('FuseJS/CameraRoll');
var Environment = require('FuseJS/Environment');
var FileSystem = require('FuseJS/FileSystem');
var Base64 = require('FuseJS/Base64');
var Config = require('../Config');
var UploadContext = require('Modules/UploadContext');

var states = {
  IMAGE_NOT_SELECTED: 'imageNotSelected',
  IMAGE_SELECTED: 'imageSelected',
  UPLOADING_IMAGE: 'uploadImage',
  UPLOADING_IMAGE_COMPLETE: 'uploadingImageComplete'
};

var UploadStates = Object.freeze({
  IDLE: 'idle',
  CONVERTING: 'converting',
  UPLOADING: 'uploading',
  UPLOAD_COMPLETE: 'uploadComplete',
  DONE: 'done',
  ERROR: 'error'
});

// /** @type {IObservable} */
// var image = Observable();
/** @type {IObservable} */
var image = UploadContext.image;

/** @type {IObservable} */
var uploadedImage = UploadContext.uploadedImage;

var getUploadedImage = function () {
  return uploadedImage.value;
};

var setUploadedImage = function (img) {
  uploadedImage.value = img;
};

/** @type {IObservable} */
var uploadState = Observable(UploadStates.IDLE);

image.addSubscriber(function (x) {
  // console.log('Observable<image> updated with: ' + JSON.stringify(x));
});

var flags = {
  hasImageSelected: image.count().map(R.gt(R.__, 0)),
  isUploading: Observable(false)
};

var setUploadState = function (newState) {
  uploadState.value = newState;
};

var getUploadState = function () {
  return uploadState.value;
};

var uploadImage = function (imageObject) {
  setUploadState(UploadStates.CONVERTING);

  var endpointUrl = Config.imgur.baseUrl + '/image';
  var params = {
    method: 'POST',
    headers: new Headers({
      'Authorization': 'Client-ID ' + Config.imgur.auth.clientId,
      'Content-Type': 'application/json'
    }),
    body: JSON.stringify({
      image: Base64.encodeBuffer(FileSystem.readBufferFromFileSync(imageObject.path)),
      type: 'base64'
    })
  };

  setUploadState(UploadStates.UPLOADING);

  return fetch(endpointUrl, params)
    .then(function (res) {
      setUploadState(UploadStates.UPLOAD_COMPLETE);

      if (res.ok) {
        return res.json();
      }
      else if (res.status === 400) {
        throw new Error('sent a bad request');
      }
      else if (res.status === 1040) {
        throw new Error('imgur is over capacity, try again later');
      }
      else {
        throw new Error('response was not OK');
      }
    })
    .then(function (x) {
      console.log('TODO: Upload.js — Implement saving to backend ');
      console.log('- ' + JSON.stringify(x, null, 2));
      return x;
    })
    .then(function (json) {
      setUploadState(UploadStates.DONE);
      setUploadedImage(json.data);
      router.goto('upload', {}, 'result', { data: getUploadedImage() });
    })
    .catch(function (err) {
      console.log('Caught error!');
      setUploadState('error');
      if (err instanceof Error) {
        console.log('Error: ' + err.name);
        console.log('Msg:   ' + err.message);
        console.log('Stack: ' + err.stack);
      }
      else {
        console.log(JSON.stringify(err));
      }
    });
};

// module.exports = {
//   flags: flags,
//
//   stateCallbacks: {
//     uploading: function (x) {
//       console.log('`stateCallbacks.uploading` called with: ' + JSON.stringify(x));
//     },
//     done: function (x) {
//       if (!uploadedImage.value) {
//         console.log('stateCallbacks.done: No image uploaded. Probably currently testing animation.');
//         return;
//       }
//       console.log('`stateCallbacks.done` called with: ' + JSON.stringify(x.data));
//       console.log('uploaded image: ' + JSON.stringify(uploadedImage.value));
//       router.push('upload', {}, 'result', { data: uploadedImage.value });
//     }
//   },
//
//   actions: {
//     debug: function () {
//       console.log('debug function');
//     },
//     reset: function () {
//       console.log('Clear current image.');
//       image.clear();
//     },
//     doUpload: function (sender) {
//       uploadState.value = 'preparing';
//       uploadImage(image.value);
//     },
//     selectImage: function (sender) {
//       console.log('clicked selectImage');
//
//       // Add a dummy image if we're currently developing stuff
//       if (Environment.desktop) {
//         image.value = {
//           path: 'Assets/Images/rahat.jpg',
//           name: 'rahat.jpg',
//           width: 720,
//           height: 432,
//           info: {}
//         };
//
//         router.goto('upload', {}, 'confirm', { image: image.value });
//       }
//       else {
//         var imagePromise = CameraRoll.getImage();
//         imagePromise.then(function (imageObject) {
//           console.log('got image: ' + JSON.stringify(imageObject));
//           image.value = imageObject;
//         });
//       }
//     }
//   }
// };
