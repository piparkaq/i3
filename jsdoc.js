/**
 * @fileoverview
 * @module jsdoc
 * @since 0.1.0
 */

/**
 * @typedef {object} HistoryListSegmentModel
 * @prop {string} title
 * @prop {string} [timespanStart]
 * @prop {string} [timespanEnd]
 * @prop {Array<HistoryEntryModel>} entries
 */

/**
 * @typedef {object} HistoryEntryModel
 * @prop {string} id
 * @prop {createdAt} string
 * @prop {modifiedAt} string
 * @prop {*} originalData
 */
