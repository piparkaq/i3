/**
 * @fileoverview
 * @module Config
 * @since 0.1.0
 */
declare module Config {
  export interface db {
    centralDatabaseFile: string;
    entryDirectory: string;
  }
  export interface imgur {
    baseUrl: string;
    auth: {
      clientId: string;
      clientSecret: string;
    };
  }
}

declare module 'Config' {
  export default Config;
}
