/**
 * @fileoverview
 * @module MainView
 * @since 0.1.0
 */
'use strict';

require('Modules/Polyfills');

var Settings = require('Modules/Settings');

var initChain = [
  Settings.actions.initialize
];

if (initChain.length > 0) {
  initChain.forEach(function (fn) {
    fn.apply(fn);
  })
}
