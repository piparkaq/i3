/**
 * @fileoverview
 * @module Modules/Utils
 * @since 0.1.0
 */
'use strict';

var FileSystem = require('FuseJS/FileSystem');

/**
 * @returns {string}
 */
function guid () {
  function s4 () {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
  }

  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

/**
 * @param {string} message
 */
function logError (message) {
  var extraMessage = message ? '; {0}'.format(message) : '';
  return function (err) {
    if (err) {
      console.log('Caught error{0}'.format(extraMessage));

      if (err instanceof Error) {
        console.log('Error: {0}'.format(err.name));
        console.log(' - {0}'.format(err.message));
        console.log('Stack trace:\n\t{0}'.format(err.stack));
      }
      else {
        console.log('Error: {0}'.format(JSON.stringify(err)));
      }
    }
  }
}

/**
 * @param {string} file
 * @returns {string}
 */
function getPath (file) {
  return '{0}/{1}'.format(FileSystem.dataDirectory, file);
}

module.exports = {
  guid: guid,
  logError: logError,
  getPath: getPath
};
