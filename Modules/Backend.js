/// <reference path="Backend.d.ts" />
/**
 * @fileoverview
 *  The "back-end" implementation for the application.
 *
 *  This module will ensure that the required structure on the filesystem will exist,
 *  and ties in with the [Context module]{@link Modules/Context}.
 *
 *  If you require saving data to disk, and it will be used in the application, consider
 *  using the [Context module]{@link Modules/Context} methods, so that the application
 *  UI can be better kept at the correct and wanted state.
 *
 * @todo Add possible integration to a cloud service to back up application data.
 *
 * @module Modules/Backend
 * @namespace Modules.Backend
 * @since 0.1.0
 */
'use strict';

var I = require('immutable');
var R = require('ramda');
var Promise = require('bluebird');
var FileSystem = require('FuseJS/FileSystem');

var Utils = require('./Utils');
var Config = require('../Config');

var guid = Utils.guid;
var logError = Utils.logError;
var getPath = Utils.getPath;

//var dataPath = '{0}/{1}'.format(FileSystem.dataDirectory, Config.db.entryDirectory);
var dataPath = getPath(Config.db.entryDirectory);
var settingsPath = getPath(Config.settings.file);

// Ensure dataPath exists
Promise.resolve(dataPath)
       .then(FileSystem.createDirectory)
       .catch(logError);

var returnIdentity = R.identity(R.__);

/**
 * @type {Modules.Backend.writeEntry}
 * @param {{}} entry
 * @return {Promise<*>}
 */
var writeEntry = function Backend$writeEntry (entry) {
  var targetFile = '{1}/{0}.json'.format(entry.id, dataPath);
  var serializedData = JSON.stringify(entry);

  console.log('>> Writing entry {0} to disk'.format(targetFile));
  return Promise.resolve(entry)
                .then(FileSystem.writeTextToFile(targetFile, serializedData));
};

/**
 * @type {function}
 * @param {string} id
 * @return {Promise<*>}
 */
var readEntryById = function Backend$readEntryById (id) {
  var filePath = '{0}/{1}.json'.format(dataPath, id);
  return Promise.resolve(filePath)
                .then(FileSystem.readTextFromFile)
                .then(JSON.parse);
};

/**
 * @type {function}
 * @param {string} file
 * @return {Promise<*>}
 */
var readJsonFromFile = function Backend$readJsonFromFile (file) {
  return Promise.resolve(file)
                .then(FileSystem.readTextFromFile)
                .then(JSON.parse);
};

/**
 * @type {function}
 * @param {string} file
 * @param {string} data
 */
var writeJsonToFile = function Backend$writeJsonToFile (file, data) {
  return Promise.resolve(data)
                .then(JSON.stringify)
                .then(R.partial(FileSystem.writeTextToFile, [file]));
}

/**
 * @type {typeof Modules.Backend.actions}
 */
var actions = {
  settings: {
    /**
     * Initialize settings by ensuring the settings file exists, and is
     * populated by the default settings.
     * @returns {Promise<*>}
     */
    initialize: function Backend$actions$settings$initialize () {
      return Promise.resolve(null);
    },
    /**
     * @type {function}
     * @returns {Promise<*>}
     */
    read: function Backend$actions$settings$read () {
      return Promise.resolve(settingsPath)
                    .then(readJsonFromFile)
    },
    /**
     * @type {function}
     * @param {*} data
     * @returns {Promise<void>}
     */
    write: function (data) {
      return Promise.resolve(data)
                    .then(writeJsonToFile)
    }
  },
  /**
   * @type {function}
   * @returns {Promise<Array<*>>}
   */
  readEntries: function Backend$actions$readEntries () {
    return Promise.resolve(dataPath)
                  .then(FileSystem.listFiles)
                  .map(readJsonFromFile)
                  .then(function (entries) {
                    console.log('>> Read {0} entries from {1}'.format(entries.length, dataPath));
                    return entries;
                  });
  },
  /**
   * @type {function}
   * @param {{}} entry
   * @returns {Promise<{id: string, createdAt: string, modifiedAt: string, originalData: any}>}
   */
  createEntry: function Backend$actions$createEntry (entry) {
    var id = guid();
    var newFields = { id: id, createdAt: +(new Date()), modifiedAt: +(new Date()) };
    var newEntry = Object.assign({}, entry, newFields);

    return Promise.resolve(newEntry)
                  .then(writeEntry)
                  .then(returnIdentity(newEntry));
  },
  /**
   * @type {function}
   * @param {{}} entry
   * @return {Promise<*>}
   * @todo Please finish me
   */
  updateEntry: function Backend$actions$updateEntry (entry) {
    // Empty
  },
  /**
   * @type {function}
   * @param {string} id
   * @return {Promise<*>}
   * @todo Please finish me
   */
  deleteEntry: function Backend$actions$deleteEntry (id) {
    // Empty
  }
};

module.exports = {
  actions: actions
};
