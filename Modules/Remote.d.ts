/**
 * @fileoverview
 * @module Modules/Remote
 * @since 0.1.0
 */
import Promise from 'bluebird';

declare namespace i3 {
  export namespace Modules {
    export module Remote {
      export function uploadImage(image: any): Promise;
    }
  }
}

declare module 'Modules/Remote' {
  import Remote = i3.Modules.Remote;
  export default Remote;
}
