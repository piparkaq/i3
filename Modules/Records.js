/// <reference path="Records.d.ts" />
/**
 * @fileoverview
 * @module Modules/Records
 * @namespace Modules.Records
 * @since 0.1.0
 */
'use strict';

var I = require('immutable');

/**
 * @type {SettingFieldRecord}
 */
module.exports.SettingFieldRecord = I.Record({
  id: undefined,
  title: undefined,
  description: undefined,
  type: undefined,
  value: undefined,
  placeholderString: undefined,
  group: undefined
});

/**
 * @type {SettingGroupRecord}
 */
module.exports.SettingGroupRecord = I.Record({
  name: undefined,
  shortName: undefined,
  description: undefined,
  fields: I.List()
});

/**
 * @type {ApplicationSettingsRecord}
 */
module.exports.ApplicationSettingsRecord = I.Record({
  timestampFormatLShort: 'YYYY-MM-DD H:mm:ss',
  timestampFormatLong: 'dddd, Do MMMM, YYYY H:mm:ss'
});
