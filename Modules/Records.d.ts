/**
 * @fileoverview
 * @module Modules/Records
 * @namespace Modules.Records
 * @since 0.1.0
 */
import { List } from 'immutable';

declare namespace Modules.Records {
  export interface SettingFieldRecord {
    id: string;
    groupShortName: string;
    title: string;
    description: string;
    type: 'string' | 'float' | 'boolean';
    value: string | number | boolean;
    placeholderString: string;
  }

  export interface SettingGroupRecord {
    name: string;
    shortName: string;
    description: string;
    fields: List<SettingFieldRecord>;
  }

  export interface ApplicationSettingsRecord {
    /**
     * Format for short dates
     */
    timestampFormatShort: string;
    /**
     * Format for long dates
     */
    timestampFormatLong: string;
    /**
     * Whether timestamps should be displayed as relative, e.g. "20 hours ago"
     */
    showTimestampsAsRelative: boolean;
  }
}

declare module 'Modules/Records' {
  import Records = Modules.Records;
  export default Records;
}
