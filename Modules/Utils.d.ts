/**
 * @fileoverview
 * @module Modules/Utils
 * @since 0.1.0
 */
declare namespace Modules {
  export class Utils {
    guid(): string;
    logError(message?: string): (err: Error | String) => void;
  }
}

declare module 'Modules/Utils' {
  import Utils = Modules.Utils;
  export default Utils;
}
