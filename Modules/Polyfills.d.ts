/**
 * @fileoverview
 * @module Modules/Polyfills
 * @since 0.1.0
 */
declare namespace i3 {
  export namespace Modules {
    export module Polyfills {}
  }
}

declare module 'Modules/Polyfills' {
  import Polyfills = i3.Modules.Polyfills;
  export default Polyfills;
}
