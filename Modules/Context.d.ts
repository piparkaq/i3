/**
 * @fileoverview
 * @module Modules/Context
 * @since 0.1.0
 */
declare namespace Modules.Context {
  export namespace actions {
    export function createEntry(entry: any): void;
    export function updateEntry(entry: any): void;
    export function deleteEntry(id: string): void;
  }
}

declare module 'Modules/Context' {
  import Context = Modules.Context;
  export default Context;
}
