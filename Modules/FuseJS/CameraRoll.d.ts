/**
 * @fileoverview
 * @module FuseJS/CameraRoll
 * @since 0.1.0
 */
import Promise from 'bluebird';

declare interface CameraRollImage {
  path: string;
  name: string;
  width: number;
  height: number;
  info: any;
}

declare namespace FuseJS {
  export module CameraRoll {
    export function getImage(): Promise<CameraRollImage>;
  }
}

declare module 'FuseJS/CameraRoll' {
  import CameraRoll = FuseJS.CameraRoll;
  export default CameraRoll;
}
