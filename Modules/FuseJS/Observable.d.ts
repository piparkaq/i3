/**
 * @fileoverview Typescript definitions for the FuseJS/IObservable module
 * @version 0.1.0
 */

declare module 'FuseJS.Observable' {
  export default Observable;
}

/**
 * Constructs a new observable with the given value(s)
 * @template T
 * @param values
 * @constructor
 */
export function Observable<T>(...values: T[]): IObservable<T>;

export interface IObservable<T> {
  value: T;
  /**
   * Concatenate the contents of the Observable into a single array
   */
  toArray(): Array<T>;
  length: number;

  /**
   * Add value to the Observable
   * @param value
   */
  add(value: T): void;
  /**
   * Concatenate the given iterable's content to the content of the Observable
   * @param values
   */
  addAll(values: Array<T>): void;

  // List operators
  clear(): void;
  contains(value: T): boolean;
  forEach(item: T, index?: number): void;
  getAt(index: number): T;
  identity(): IObservable<T>;
  indexOf(value: T): number;
  insertAll(index: number, array: Array<T>): void;
  insertAt(index: number, value: T): void;
  refreshAll(
    newValues: Array<T>,
    compareFunc: (oldValue: T, newValue: T) => boolean,
    updateFunc: (oldValue: T, newValue: T) => void,
    mapFunc: (newValue: T) => T): void

  removeValue(value: T): void;
  /**
   * Remove the value at the given index from the Observable
   * @param index
   */
  removeAt(index: number): void;
  /**
   * Remove values from a range (inclusive)
   * @param start
   * @param count
   */
  removeRange(start: number, count: number): void;
  /**
   * Remove all values that fulfill the given predicate
   * @param func
   */
  removeWhere(func: (value: T) => boolean);
  replaceAll(array: Array<T>): void;
  replaceAt(index: number, value: T): void;
  tryRemove(value: T): boolean;

  // Reactive operators
  any(value: T): IObservable<boolean>;
  combine(obs: Array<IObservable<T>>, func: (...values: T[]) => T): IObservable<T>;
  combineLatest(obs: Array<IObservable<T>>, func: (...values: T[]) => T): IObservable<T>;
  combineArrays(obs: Array<IObservable<T>>, func: (...values: T[]) => T): IObservable<T>;
  /**
   * Return an {@link IObservable<number>} that will reflect the number of values
   * in the original {@link IObservable<T>}
   */
  count(): IObservable<number>;
  count(condition: (value: T) => boolean): IObservable<T>;
  expand(func: (value: Array<T>) => T[]): IObservable<T>;
  filter(condition: (value: T) => boolean): IObservable<T>;
  first(): IObservable<T>;
  flatMap(func: (item: T) => T): IObservable<T>;
  inner(): IObservable<T>;
  last(): IObservable<T>;
  map<TInput, TOutput>(func: (value: TInput, index?: number) => TOutput): IObservable<TOutput>;
  not(): IObservable<T>;
  pick(index: number): IObservable<T>;
  setInnerValue(value: T): IObservable<T>;
  slice(begin?: number, end?: number);
  twoWayMap<T, U>(getter: (value: T | U) => T | U, setter: (value: T | U) => T | U): IObservable<T | U>;
  where(condition: (value: T) => T): IObservable<T>;
  where(condition: { [key: string]: any }): IObservable<T>;

  // Subscribers
  /**
   * Add a subscriber function that will be called on every change to the Observable
   * @param func
   */
  addSubscriber(func: (value: T) => void): void;
  /**
   * Remove a subscriber function from the Observable
   * @param func
   */
  removeSubscriber(func: (value: T) => void): void;

  // Other
  setValueWithOrigin(value: T, origin: T): void;
  /**
   * Convert the Observable into a {@link string} representation of its contents
   */
  toString(): string;

  /** @private */
  _CompareFn<TInput, TOutput>(oldValue: TInput, newValue: TOutput): boolean;

  /** @private */
  _UpdateFn<TInput, TOutput>(oldValue: TInput, newValue: TOutput): void;

  /** @private */
  _MapFn<TInput, TOutput>(newValue: TInput): TOutput;
}
