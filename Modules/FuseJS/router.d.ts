/**
 * @fileoverview
 * @module FuseJS/Global
 * @since 0.1.0
 */
declare module router {
  export function goto(...params: any[]): void;
  export function push(...params: any[]): void;
  export function goBack(): void;
  export function getRoute(callback: (...params: any[]) => void): void;
  export function gotoRelative(node: any, ...params: any[]): void;
  export function pushRelative(node: any, ...params: any[]): void;
  export function modify(navigationSpec: INavigationSpec): void;

  export type NavigationType = 'Goto' | 'Push' | 'Replace';
  export type TransitionType = 'Transition' | 'Bypass';

  interface INavigationSpec {
    how: NavigationType,
    path: any[],
    transition: TransitionType
  }
}
