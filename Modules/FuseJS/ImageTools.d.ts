/**
 * @fileoverview
 * @module FuseJS/ImageTools
 * @since 0.1.0
 */
import Promise from 'bluebird';

declare namespace FuseJS {
  export module ImageTools {
    // Interfaces
    export interface IImage {
      path: string;
    }

    export interface ICropOptions {
      x: number;
      y: number;
      width: number;
      height: number;
      /**
       * Should the operation replace the existing image instead of creating a new one
       */
      performInPlace: boolean;
    }

    // Operations
    /**
     * Gets the contents of the specified {@link IImage} as Base64
     * @param image
     */
    export function getBase64FromImage(image: IImage): string;
    /**
     * Gets the content of the specified {@link IImage} as a {@link Buffer}
     * @param image
     */
    export function getBufferFromImage(image: IImage): Buffer;
    /**
     * Performs a cropping operation on the specified {@link IImage} with the given
     * {@link ICropOptions} and returns a {@link Promise} of the result
     * @param image
     */
    export function crop(image: IImage, options: ICropOptions): Promise<IImage>;
    /**
     * Gets an {@link IImage} from the given Base64 string
     * @param image
     */
    export function getImageFromBase64(image: string): IImage;
    /**
     * Gets an {@link IImage} from the given {@link Buffer}
     * @param image
     */
    export function getImageFromBuffer(image: Buffer): IImage;
  }
}

declare module 'FuseJS/ImageTools' {
  import ImageTools = FuseJS.ImageTools;
  export default ImageTools;
}
