/**
 * @fileoverview
 * @module FuseJS/Storage
 * @since 0.1.0
 */
'use strict';

var R = require('ramda');
var Storage = require('FuseJS/Storage');
var createError = require('Modules/Funcs/createError');

var FileNotFoundError = createError('FileNotFoundError', 'the file could not be found');
var FileNotWritableError = createError('FileNotWritableError', 'the file could not be written');

var getError = R.cond([
  [R.equals('File could not be found.'), R.always(FileNotFoundError)]
]);

var rejectWithPossibleError = R.curry(function (reject, err) {
  var ErrorObject = getError(err);
  var result = err;
  if (ErrorObject) {
    result = new ErrorObject(err);
  }

  return result;
});

var getThrowable = R.cond([
  [R.is(Function), R.identity],
  [R.is(Error), R.construct],
  [R.is(String), R.compose(R.construct, R.call(getError))]
]);

/**
 * @alias module:FuseJS/Storage.write
 * @type {function}
 * @param {string} file
 * @param {string} data
 * @returns {Promise<boolean>}
 */
var write = module.exports.write = function FuseJS$Storage$write (file, data) {
  return Promise.resolve(Storage.write(file, data))
                .then(resolve)
                .catch(rejectWithPossibleError(reject));
};

/**
 * @alias module:FuseJS/Storage.writeSync
 * @type {function}
 * @param {string} file
 * @param {string} data
 * @returns {boolean}
 */
var writeSync = module.exports.writeSync = function FuseJS$Storage$writeSync (file, data) {
  try {

  }
  catch (err) {
    if (R.is(Function, err)) {
      throw err;
    }
    else if (R.is(String, err)) {
      var ErrorObject = getError(err);
      if (ErrorObject) {
        throw new ErrorObject(err);
      }
    }
  }
};

/**
 * @alias module:FuseJS/Storage.read
 * @type {function}
 * @param {string} file
 * @returns {Promise<string>}
 */
var read = module.exports.read = function FuseJS$Storage$read (file) {

};

/**
 * @alias module:FuseJS/Storage.readSync
 * @type {function}
 * @param {string} file
 * @returns {string}
 */
var readSync = module.exports.readSync = function FuseJS$Storage$readSync (file) {

};
