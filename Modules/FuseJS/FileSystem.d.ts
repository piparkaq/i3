/**
 * @fileoverview
 * @module FuseJS/FileSystem
 * @namespace FuseJS.FileSystem
 * @since 0.1.0
 */
import Promise from 'bluebird';

declare namespace FuseJS {
  export module FileSystem {
    export const androidPaths: string[][];
    export const iosPaths: string[][];
    export const cacheDirectory: string;
    export const dataDirectory: string;

    // Read and write operations
    export function readBufferFromFile(path: string): Promise<ArrayBuffer>;
    export function readBufferFromFileSync(path: string): ArrayBuffer;
    export function readTextFromFile(path: string): Promise<string>;
    export function readTextFromFileSycn(path: string): string;
    export function writeBufferToFile(path: string, data: ArrayBuffer): Promise<void>;
    export function writeBufferToFileSync(path: string, data: ArrayBuffer): void;
    export function writeTextToFile(path: string, data: string): Promise<void>;
    export function writeTextToFileSync(path: string, data: string): void;
    export function appendTextToFile(path: string, data: string): Promise<void>;
    export function appendTextToFileSync(path: string, data: string): void;

    // File and directory operations
    export function move(source: string, destination: string): Promise<void>;
    export function moveSync(source: string, destination: string): void;

    // File operations
    export function getFileInfo(path: string): Promise<{ size: number, exists: boolean, lastWriteTime: string, lastAccessTime: string }>;
    export function getFileInfoSync(path: string): { size: number, exists: boolean, lastWriteTime: string, lastAccessTime: string };
    //export function delete(path: string): Promise<void>;
    export function deleteSync(path: string): void;
    export function exists(path: string): Promise<boolean>;
    export function existsSync(path: string): boolean;

    // Directory operations
    export function getDirectoryInfo(path: string): Promise<{ exists: boolean, lastWriteTime: string, lastAccessTime: string }>;
    export function getDirectoryInfoSync(path: string): { exists: boolean, lastWriteTime: string, lastAccessTime: string };
    export function createDirectory(path: string): Promise<void>;
    export function createDirectorySync(path: string): void;
    export function listDirectories(path: string): Promise<Array<string>>;
    export function listDirectoriesSync(path: string): Array<string>;
    export function listEntries(path: string): Promise<Array<string>>;
    export function listEntriesSync(path: string): Array<string>;
    export function listFiles(path: string): Promise<Array<string>>;
    export function listFilesSync(path: string): Array<string>;
  }
}

declare module 'FuseJS/FileSystem' {
  import FileSystem = FuseJS.FileSystem;
  export default FileSystem;
}
