/**
 * @fileoverview
 * @module FuseJS/ComponentContext
 * @since 0.1.0
 */
declare namespace FuseJS {
  export module ComponentContext {
    export type Parameter = { [key: string]: any } | string;
  }
}

declare module FuseComponentContext {
  export type Parameter = { [key: string]: any } | string;
}
