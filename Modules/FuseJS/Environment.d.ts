/**
 * @fileoverview
 * @module FuseJS/Environment
 * @since 0.1.0
 */
declare namespace FuseJS {
  export module Environment {
    export const android: boolean;
    export const ios: boolean;
    export const desktop: boolean;
    export const preview: boolean;
    export const mobile: boolean;
    export const mobileOSVersion: string;
  }
}

declare module 'FuseJS/Environment' {
  import Environment = FuseJS.Environment;
  export default Environment;
}
