/**
 * @fileoverview
 * @module FuseJS/Base64
 * @since 0.1.0
 */
declare namespace FuseJS {
  export module Base64 {
    export function encodeBuffer(buffer: ArrayBuffer): string;
  }
}

declare module 'FuseJS/Base64' {
  import Base64 = FuseJS.Base64;
  export default Base64;
}
