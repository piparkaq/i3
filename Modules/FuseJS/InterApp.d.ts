/**
 * @fileoverview
 * @module FuseJS/InterApp
 * @since 0.1.0
 */
declare namespace FuseJS {
  export module InterApp {
    export function launchUri(uri: string): void;
  }
}

declare module 'FuseJS/InterApp' {
  import InterApp = FuseJS.InterApp;
  export default InterApp;
}
