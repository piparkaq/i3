/**
 * @fileoverview
 * @module FuseJS/Storage
 * @namespace FuseJS.Storage
 * @since 0.1.0
 */
import Promise from 'bluebird';

declare namespace FuseJS.Storage {
  export function deleteSync(): void;
  export function write(): Promise<boolean>;
  export function writeSync(): boolean;
  export function read(): Promise<string>;
  export function readSync(): string;
}

declare module 'FuseJS/Storage' {
  import Storage = FuseJS.Storage;
  export default Storage;
}
