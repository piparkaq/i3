/**
 * @fileoverview
 * @module Modules/Settings
 * @namespace Modules.Settings
 * @since 0.1.0
 */
export namespace Modules.Settings {
  export interface Application {

  }
}

declare module 'Modules/Settings' {
  import Settings = Modules.Settings;
  export default Settings;
}
