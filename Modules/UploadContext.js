/* global router: true */
/**
 * @fileoverview
 * @module Modules/UploadContext
 * @since 0.1.0
 */
'use strict';

var Promise = require('bluebird');
var Observable = require('FuseJS/Observable');
var Environment = require('FuseJS/Environment');
var CameraRoll = require('FuseJS/CameraRoll');

// Expose image object
/** @type {IObservable} */
var image = Observable();

var setImage = function (data) {
  image.value = data;
};

var getImage = function () {
  return image.value;
};

// Expose uploaded image object
/** @type {IObservable} */
var uploadedImage = Observable();

var setUploadedImage = function (data) {
  uploadedImage.value = data;
};

var getUploadedImage = function () {
  return uploadedImage.value;
};

// Actions
var actions = {
  clearContext: function () {
    image.clear();
    uploadedImage.clear();
  },
  selectImage: function () {
    // Add a dummy image if we're currently developing stuff
    if (Environment.desktop) {
      setImage({
        path: 'Assets/Images/rahat.jpg',
        name: 'rahat.jpg',
        width: 720,
        height: 432,
        info: {}
      });

      return Promise.resolve(getImage());
    }
    else {
      var imagePromise = CameraRoll.getImage();
      return imagePromise.then(function (imageObject) {
        console.log('got image: ' + JSON.stringify(imageObject));
        setImage(imageObject);
        return getImage();
      });
    }
  }
};

module.exports = {
  image: image,
  getImage: getImage,
  setImage: setImage,

  uploadedImage: uploadedImage,
  getUploadedImage: getUploadedImage,
  setUploadedImage: setUploadedImage,

  actions: actions
};
