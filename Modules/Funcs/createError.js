/**
 * @fileoverview
 * @module Modules/Funcs/createError
 * @since 0.1.0
 */

/**
 * @param {string} name
 * @param {string} [defaultMessage]
 * @returns {Error}
 */
function createError (name, defaultMessage) {
  function CustomError (message) {
    this.name = name;
    this.message = message || defaultMessage;
    this.stack = (new Error()).stack;
  }

  CustomError.prototype = Object.create(Error.prototype);
  CustomError.prototype.constructor = CustomError;

  return CustomError;
}

module.exports = createError;
