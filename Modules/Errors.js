/**
 * @fileoverview
 * @module Modules/Error
 * @since 0.1.0
 */
'use strict';

var R = require('ramda');
var createError = require('./Funcs/createError');

var errors = [
  ['ImgurOverCapacityError', 'imgur is over capacity'],
  ['ImgurResponseNotOkError', 'The response received was not OK'],
  ['HttpBadRequestError', 'HTTP1.1/400 Bad Request']
];

/**
 * @type {{ [key: string]: Error }}
 * @alias module:Modules/Error
 */
module.exports = R.fromPairs(errors.map(function (p) {
  return [p[0], createError.apply(createError, p)];
}));
