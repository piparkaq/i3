/**
 * @fileoverview
 * @module Modules/Logger
 * @since 0.1.0
 */
'use strict';

var R = require('ramda');

var createLogger = R.curry(function createLogger (options, level, prefix, str) {
  console.log(level + ': ' + str);
});

var info = createLogger(null, 'info', null);

module.exports = {
  createLogger: createLogger,

  info: info
};
