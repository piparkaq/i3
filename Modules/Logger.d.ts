/**
 * @fileoverview
 * @module Modules/Logger
 * @since 0.1.0
 */
declare namespace i3 {
  export namespace Modules {
    export module Logger {}
  }
}

declare module 'Modules/Logger' {
  import Logger = i3.Modules.Logger;
  export default Logger;
}
