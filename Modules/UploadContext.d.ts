/**
 * @fileoverview
 * @module Modules/UploadContext
 * @since 0.1.0
 */
declare namespace i3 {
  export namespace Modules {
    export module UploadContext {
    }
  }
}

declare module 'Modules/UploadContext' {
  import UploadContext = i3.Modules.UploadContext;
  export default UploadContext;
}
