/**
 * @fileoverview
 * @module Modules/Backend
 * @since 0.1.0
 */
import Promise from 'bluebird';

declare namespace Modules {
  interface BackendActions {
    settings: {
      /**
       * Initialize settings by ensuring the settings file exists, and is
       * populated by the default settings.
       */
      initialize(): Promise<any>;
      /**
       * Read settings from file and return it deserialized into JSON
       */
      read(): Promise<any>;
      /**
       * Write settings as serialized JSON data to file
       * @param data
       */
      write(data: any): Promise<void>;
    };

    /**
     * Create a new entry that will be saved to disk. Some parameters will be overwritten if
     * they are defined in the source object.
     *
     * Generates the following fields: id, createdAt, modifiedAt
     * @param entry
     */
    createEntry(entry: { originalData: any }): Promise;

    /**
     * Update an entry with the given object. Will look up the given entry based on the `id` found
     * in the entry.
     * @param entry
     */
    updateEntry(entry: { id: string, createdAt: string, modifiedAt: string, originalData: any }): Promise;
    // export function updateEntry(id: string, entry: {}): Promise;

    /**
     * Deletes an entry with the given `id` from disk.
     * @param id
     */
    deleteEntry(id: string): Promise;
  }

  export class Backend {
    private static writeEntry(entry: any): Promise;
    private static readEntry(file: string): Promise;
    private static readEntryById(id: string): Promise;
    public static actions: BackendActions;
  }
}

declare module 'Modules/Backend' {
  import Backend = Modules.Backend;
  export default Backend;
}
