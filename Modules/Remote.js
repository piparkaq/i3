/**
 * @fileoverview
 * @module Modules/Remote
 * @since 0.1.0
 */
'use strict';

var FileSystem = require('FuseJS/FileSystem');
var Base64 = require('FuseJS/Base64');
var Config = require('../Config');
var Errors = require('./Errors');

var upload = function (data) {
  var endpointUrl = Config.imgur.baseUrl + '/image';
  var params = {
    method: 'POST',
    headers: new Headers({
      'Authorization': 'Client-ID ' + Config.imgur.auth.clientId,
      'Content-Type': 'application/json'
    }),
    body: JSON.stringify({
      image: Base64.encodeBuffer(FileSystem.readBufferFromFileSync(data.path)),
      type: 'base64'
    })
  };

  return fetch(endpointUrl, params)
    .then(function (res) {
      if (res.ok) {
        return res.json();
      }
      else if (res.status === 400) {
        throw new Errors.HttpBadRequestError();
      }
      else if (res.status === 1040) {
        throw new Errors.ImgurOverCapacityError();
      }
      else {
        throw new Errors.ImgurResponseNotOkError();
      }
    });
};

var actions = Object.freeze({
  upload: upload
});

module.exports = {
  actions: actions
};
