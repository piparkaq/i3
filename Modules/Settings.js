/**
 * @fileoverview
 * @module Modules/Settings
 * @namespace Modules.Settings
 * @since 0.1.0
 */
'use strict';

var I = require('immutable');
var Backend = require('Modules/Backend');
var Records = require('Modules/Records');

var shortTimestampFormat = 'YYYY-MM-DD H:mm:ss';
var longTimestampFormat = 'dddd, Do MMMM, YYYY H:mm:ss';

var settingGroups;

var actions = {
  initialize: function () {
    // Backend.settings.initialize();
  }
};

module.exports = {
  actions: actions
};
