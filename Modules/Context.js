/// <reference path="Context.d.ts" />
/**
 * @fileoverview
 *  Defines a usable context for the UI. This will act as the single source of truth
 *  when it comes to data.
 *
 * @module Modules/Context
 * @namespace Modules.Context
 * @since 0.1.0
 */
'use strict';

var I = require('immutable');
var Observable = require('FuseJS/Observable');
var Backend = require('./Backend');

/**
 * @type {IObservable}
 * @alias module:Modules/Context.entries
 */
var entries = module.exports.entries = Observable();

/**
 * Read saved entries from the back-end and populate the observable
 * instance that will act as the data context in this application.
 */
Backend.actions
       .readEntries()
       .then(function (data) {
         entries.replaceAll(data);
       });

/**
 * @alias module:Modules/Context.actions
 */
var actions = module.exports.actions = {
  /**
   * @type {Modules.Context.actions.createEntry}
   * @param entry
   */
  createEntry: function (entry) {
    return Backend.actions
                  .createEntry(entry)
                  .then(function (entry) {
                    entries.add(entry);
                  });
  },
  /**
   * @type {function}
   * @param {string} id
   * @param {*} entry
   */
  updateEntry: function (id, entry) {
    entries.forEach(function (it, idx) {
      var currentEntry = entries.getAt(idx);
      if (id === currentEntry.id) {
        entries.replaceAt(idx, entry);
      }
    })
  },
  /**
   * @type {Modules.Context.actions.deleteEntry}
   * @param {string} id
   */
  deleteEntry: function (id) {
    entries.forEach(function (it, idx) {
      var entry = entries.getAt(idx);
      if (id === entry.id) {
        entries.replaceAt(idx, entry);
      }
    });
  }
};
