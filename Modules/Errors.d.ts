/**
 * @fileoverview
 * @module Modules/Errors
 * @since 0.1.0
 */
declare namespace Modules {
  export class Errors {
    public static ImgurOverCapacityError: Error;
    public static HttpBadRequestError: Error;
  }
}

declare module 'Modules/Errors' {
  import Errors = Modules.Errors;
  export default Errors;
}
