
# i3 — the Improved Imgur Interface

## Development

Requirements:

 * [node.js][node_www] (recommended latest v7)
 * [Fuse 0.31][fuse_www] (or later)
 * [yarn][yarn_www] (optional)

### Setting up the environment

If `yarn` is installed:

```
yarn && yarn start
```

If `yarn` is _not_ installed:

```
npm install && npm start
```

[node_www]: https://nodejs.org/en/
[yarn_www]: https://yarnpkg.com/
[fuse_www]: https://www.fusetools.com
