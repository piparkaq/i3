/**
 * @fileoverview
 * @module Config
 * @since 0.1.0
 */
'use strict';

var config = {
  db: {
    centralDatabaseFile: 'i3.json',
    entryDirectory: 'entries'
  },
  imgur: {
    baseUrl: 'https://api.imgur.com/3',
    auth: {
      clientId: '33bf2656a4880e6',
      clientSecret: '501ea8119eb36d285d7077ecb8ead25c0a2b8870'
    }
  },
  settings: {
    file: 'i3settings.json',
    defaults: {
      timestampFormatShort: 'YYYY-MM-DD H:mm:ss',
      timestampFormatLong: 'dddd, Do MMMM, YYYY H:mm:ss',
      showTimestampsAsRelative: true
    }
  }
};

module.exports = config;
