/* global router: true */
/**
 * @fileoverview
 * @module Views/DefaultView
 * @since 0.1.0
 */
'use strict';

var Observable = require('FuseJS/Observable');
var FileSystem = require('FuseJS/FileSystem');
var Context = require('Modules/Context');

/**
 * @type {Function}
 * @param {IObservable<boolean>} observable
 */
var toggle = function (observable) {
  return function () {
    observable.value = !observable.value;
  }
};

/**
 * @type {IObservable<boolean>}
 */
var isMenuVisible = Observable(false);

/**
 * @type {IObservable<boolean>}
 */
var menuItemsTop = Observable(
  {
    text: 'Upload',
    route: ['upload', {}, 'main'],
    icon: 'FileUpload',
    isActiveRoute: false,
    isEnabled: true
  },
  {
    text: 'History',
    route: ['history', {}, 'main'],
    icon: 'History',
    isActiveRoute: false,
    isEnabled: true
  }
);

/**
 * @type {IObservable<boolean>}
 */
var menuItemsBottom = Observable(
  {
    text: 'Settings',
    route: ['settingGroups', {}, 'main'],
    icon: 'Settings',
    isActiveRoute: false,
    isEnabled: true
  }
);

var menuItems = {
  topItems: menuItemsTop,
  bottomItems: menuItemsBottom
};

/**
 * @type {function}
 * @param {object} it
 * @returns IObservable<object>
 */
var transformMenuItem = function (it) {
  return Object.assign({}, it, {
    hasIcon: it.icon != null
  });
};

var applicationMenu = {
  topMenu: menuItemsTop.map(transformMenuItem),
  bottomMenu: menuItemsBottom.map(transformMenuItem)
};

module.exports = {
  entries: Context.entries,

  isMenuVisible: isMenuVisible,
  applicationMenu: applicationMenu,

  actions: {
    toggleMenu: toggle(isMenuVisible),

    /**
     * @param {{data: Object}} sender
     */
    navigateTo: function (sender) {
      var requestedRoute = sender.data;
      Object.keys(applicationMenu).forEach(function (menu, idx) {
        applicationMenu[menu].forEach(
          /**
           * @param {{
           *  text: String,
           *  route: Array<String|Object>,
           *  hasIcon: boolean,
           *  isActiveRoute: boolean,
           *  isEnabled: boolean
           * }} it
           * @param {Number} idx
           */
          function (it, idx) {
            if (requestedRoute.route === it.route && it.isEnabled) {
              router.goto.apply(router, it.route);
              isMenuVisible.value = false;
            }
          });
      });
    }
  }
};
